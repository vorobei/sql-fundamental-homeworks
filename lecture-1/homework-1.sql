-- Show all info about CompanyName and Address from the Customers table.
SELECT company_name
, address
FROM customers

-- Show all info about CompanyName and ContactPerson from the Suppliers table.
SELECT company_name
, contact_name AS contact_person
FROM suppliers

-- Show all info about ProductName and UnitPrice from the Products table.
SELECT product_name
, unit_price
FROM products

-- Show all info about LastName, FirstName, BirthDate and HireDate of Employees.
SELECT first_name
, birth_date
, hire_date
FROM employees

-- Show all info about the employee with ID 8.
SELECT *
FROM employees
WHERE employee_id = 8

-- Show the list of first and last names of the employees from London.
SELECT first_name
, last_name
FROM employees
WHERE city IN ('London')

-- Show the list of first, last names and ages of the employees whose age is greater than 55. 
SELECT first_name
, last_name
, date_part('year', age(birth_date))::int AS employees_age
FROM employees
WHERE date_part('year', age(birth_date))::int >= 55

-- Show the list of products with the price between 10 and 50.
SELECT product_name
, unit_price AS price
FROM products
WHERE unit_price BETWEEN 10 AND 50

-- Show the list of products which names start form ‘N’ and average price is greater than 50.
SELECT product_name
, unit_price AS price
FROM products
WHERE product_name LIKE ('N%') AND unit_price > 50

-- Show the total number of employees which live in the same city.
SELECT COUNT(city)
, city
FROM employees
GROUP BY city

-- Show the list of suppliers which name begins with letter ‘A’  and are situated in London.
SELECT contact_name
, company_name
, city
FROM suppliers
WHERE city IN ('London') AND contact_name LIKE ('A%')

-- Show the list of first, last names and ages of the employees whose age is greater than average age of all employees. The result should be sorted by last name.
SELECT first_name
, last_name
, date_part('year', age(birth_date))
FROM employees
WHERE date_part('year', age(birth_date)) > (
	SELECT AVG(date_part('year', age(birth_date)))
	FROM employees
)
GROUP BY first_name, last_name, birth_date
ORDER BY last_name

-- Calculate the count of customers from Mexico and contact signed as ‘Owner’.
SELECT COUNT(company_name)
FROM customers
WHERE city LIKE ('México D.F.') AND contact_title LIKE ('Owner')

-- Write a query to get most expense and least expensive Product list (name and unit price).
SELECT product_name
, unit_price AS price
FROM products
WHERE unit_price = (SELECT MIN(unit_price) FROM products) 
   OR unit_price = (SELECT MAX(unit_price) FROM products)

-- Write a query to get Product list (name, unit price) of above average price.
SELECT product_name
, unit_price AS price
FROM products
WHERE unit_price > (SELECT AVG(unit_price) FROM products)

-- Write a query to get Product list (name, unit price) of ten most expensive products.
SELECT product_name
, unit_price AS price
FROM products
ORDER BY unit_price desc
LIMIT 10

-- For each employee that served the order (identified by EmployeeID), calculate a total Freight.
SELECT employee_id
, SUM(freight) AS total_freight
FROM orders
GROUP BY employee_id
ORDER BY employee_id

-- Calculate the greatest, the smallest and the average age among the employees from London.
SELECT MIN(date_part('year', age(birth_date))::int) AS min_age
, MAX(date_part('year', age(birth_date))::int) AS max_age
, FLOOR(AVG(date_part('year', age(birth_date))::int)) AS avarage_age
FROM employees
WHERE city IN ('London')

-- Calculate the greatest, the smallest and the average age of the employees for each city.
SELECT city
, MIN(date_part('year', age(birth_date))::int) AS min_age
, MAX(date_part('year', age(birth_date))::int) AS max_age
, FLOOR(AVG(date_part('year', age(birth_date))::int)) AS avarage_age
FROM employees
GROUP BY city

-- Show the list of cities in which the average age of employees is greater than 60 (the average age is also to be shown)
SELECT city
, FLOOR(AVG(date_part('year', age(birth_date))::int)) AS avarage_age
FROM employees
WHERE date_part('year', age(birth_date)) > 60
GROUP BY city

-- Show the first and last name(s) of the eldest employee(s).
SELECT first_name
, last_name
FROM employees
WHERE date_part('year', age(birth_date)) = (
	SELECT MAX(date_part('year', age(birth_date)))
	FROM employees
)

-- Show first, last names and ages of 3 eldest employees.
SELECT first_name
, last_name
, date_part('year', age(birth_date))
FROM employees
ORDER BY date_part('year', age(birth_date)) DESC
LIMIT 3
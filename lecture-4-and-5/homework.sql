-- Task 1.1. Show first and last names of the employees who have the biggest freight.
SELECT (E.last_name || ' ' || E.first_name) AS employee_name
FROM employees E
JOIN orders O ON E.employee_id = O.employee_id
WHERE O.freight >= (SELECT MAX(freight) FROM orders)

-- Task 1.2. Show first, last names of the employees, their freight who have the freight bigger then avarage.
SELECT (E.last_name || ' ' || E.first_name) AS employee_name
FROM employees E
JOIN orders O ON E.employee_id = O.employee_id
WHERE O.freight >= (SELECT AVG(freight) FROM orders)
GROUP BY employee_name

-- Task 1.3. Show the names of products, that have the biggest price.
SELECT product_name
FROM products P
WHERE P.unit_price >= (SELECT MAX(unit_price) FROM products)
ORDER BY unit_price DESC

-- Task 1.4. Show the name of customers with the freight bigger then avarage.
SELECT C.company_name
FROM customers C
JOIN orders O ON C.customer_id = O.customer_id
WHERE O.freight >= (SELECT AVG(freight) FROM orders)
GROUP BY C.company_name

-- Task 1.5. Show the name of supplier  who delivered the cheapest product.
SELECT S.company_name
FROM suppliers S
NATURAL JOIN products P
WHERE P.unit_price <= (SELECT MIN(unit_price) FROM products)

-- Task 2.1. Show the name of the category in which the average price of  a certain product is greater than the grand average in the whole stock.
SELECT category_name
, AVG(unit_price) AS avg_category_price
FROM categories
NATURAL JOIN products
GROUP BY category_name
HAVING AVG(unit_price) >= (SELECT AVG(unit_price) FROM products)

-- Task 2.2. Show the name of the supplier whose delivery is lower then the grand average in the whole stock.
SELECT DISTINCT company_name
FROM order_details OD
JOIN products P ON P.product_id = OD.product_id
JOIN suppliers S ON P.supplier_id = S.supplier_id
GROUP BY OD.order_id, S.company_name
HAVING SUM(OD.quantity) <= (SELECT AVG(quantity) FROM order_details)
ORDER BY S.company_name
---
SELECT DISTINCT company_name
FROM order_details OD
JOIN products P ON P.product_id = OD.product_id
JOIN suppliers S ON P.supplier_id = S.supplier_id
WHERE OD.quantity <= (SELECT AVG(units_in_stock) FROM products)

-- Task 2.3. Show the regions where employees work, the middle age of which is higher than over the whole company.
SELECT DISTINCT region_description
FROM employees E
JOIN employee_territories ET ON E.employee_id = ET.employee_id
NATURAL JOIN territories T
NATURAL JOIN region R
GROUP BY E.employee_id, region_description
HAVING date_part('year', age(E.birth_date)) > (
	SELECT AVG(date_part('year', age(birth_date))) 
	FROM employees
)

-- Task 2.4. Show customers whose maximum freight level is less than the average for all customers.
SELECT C.company_name
FROM orders
NATURAL JOIN customers C
GROUP BY C.company_name
HAVING MAX(freight) <= (SELECT AVG(freight) FROM orders)

-- Task 2.5 Show the categories of products for which the average discount is higher than the average discount for all products
SELECT category_name
FROM categories
NATURAL JOIN products
NATURAL JOIN order_details OD
GROUP BY category_name
HAVING AVG(OD.discount) >= (SELECT AVG(discount) FROM order_details)

-- Task 3.1 Calculate the average freight of all employees who work not with Western region.
SELECT AVG(freight)
FROM employees E
JOIN employee_territories ET ON E.employee_id = ET.employee_id
JOIN territories T ON ET.territory_id = ET.territory_id
JOIN region R ON T.region_id = R.region_id
JOIN orders O ON E.employee_id = O.employee_id
WHERE region_description NOT LIKE 'Western'


-- Task 3.2. Show first and last names of employees who shipped orders in cities of USA.
SELECT last_name || ' ' || first_name
FROM employees
NATURAL JOIN orders O
WHERE O.order_id = ANY (
	SELECT order_id
	FROM orders
	WHERE ship_country LIKE 'USA'
)
--
SELECT E.first_name || ' ' || E.last_name AS employee_name
FROM employees AS E 
JOIN orders AS O ON E.employee_id = O.employee_id
WHERE O.ship_country LIKE 'USA'
GROUP BY E.first_name || ' ' || E.last_name

-- Task 3.3. Show the names of products and their total cost, which were delivered by German suppliers
SELECT products.product_name
, ROUND(SUM(order_details.unit_price * order_details.quantity)::numeric, 2) AS total_cost
FROM products
JOIN order_details ON order_details.product_id = products.product_id
JOIN orders ON orders.order_id = order_details.order_id
WHERE orders.ship_country LIKE 'Germany'
GROUP BY products.product_name

-- Task 3.4. Show first and last names of employees that don’t work with Eastern region.
SELECT E.last_name || ' ' || E.first_name AS employee_name
FROM employees E
JOIN employee_territories ET ON E.employee_id = ET.employee_id
JOIN territories T ON ET.territory_id = ET.territory_id
JOIN region R ON T.region_id = R.region_id
WHERE region_description != any (
	SELECT region_description
	FROM territories
	WHERE region_description LIKE 'Eastern'
)



-- Task 3.5 Show the name of customers that prefer to order non-domestic products.
SELECT C.customer_id
FROM customers C
WHERE (
	SELECT COUNT(P.product_id)
	FROM customers CI
	NATURAL JOIN orders O
	NATURAL JOIN order_details OD
	NATURAL JOIN products P
	JOIN suppliers S ON P.supplier_id = S.supplier_id
	WHERE CI.country != S.country
	AND CI.customer_id = C.customer_id
) > (
	SELECT COUNT(P.product_id)
	FROM customers CI
	NATURAL JOIN orders O
	NATURAL JOIN order_details OD
	NATURAL JOIN products P
	JOIN suppliers S on P.supplier_id = S.supplier_id
	WHERE CI.country = S.country
	AND CI.customer_id = C.customer_id
)

-- Task 4.1 Show employees (first and last name) working with orders from the United States.
SELECT E.first_name || ' ' ||E.last_name AS employee
FROM customers C
JOIN orders O ON O.customer_id = C.customer_id
JOIN employees E ON O.employee_id = E.employee_id
WHERE C.country LIKE 'USA'
GROUP BY E.first_name || ' ' ||E.last_name

-- Task 4.2 Show the info about orders, that contain the cheapest products from USA.
SELECT *
FROM orders O
JOIN order_details OD ON OD.order_id = O.order_id
JOIN products P0 ON P0.product_id = OD.product_id 
WHERE P0.product_id = (
	SELECT P1.product_id
	FROM products P1
	WHERE P1.unit_price = (
		SELECT MIN(P2.unit_price)
		FROM products P2
		WHERE P2.supplier_id IN (
			SELECT S.supplier_id
			FROM suppliers S
			WHERE S.country LIKE 'USA'
		)
	)
)

-- Task 4.3 Show the info about customers that prefer to order meat products and never order drinks.
SELECT CUS.contact_name
FROM customers CUS
WHERE CUS.customer_id IN (
	SELECT O.customer_id
	FROM orders O
	JOIN order_details OD ON OD.order_id = O.order_id
	JOIN products P ON P.product_id = OD.product_id
	JOIN categories CA ON CA.category_id = P.category_id
	WHERE CA.category_name LIKE 'Meat/Poultry'
) AND CUS.customer_id NOT IN (
	SELECT O.customer_id
	FROM orders O
	JOIN order_details OD ON OD.order_id = O.order_id
	JOIN products P ON P.product_id = OD.product_id
	JOIN categories CA ON CA.category_id = P.category_id
	WHERE CA.category_name LIKE 'Beverages'
)
GROUP BY CUS.customer_id

-- Task 4.4 Show the list of cities where employees and customers are from and where orders have been made to. Duplicates should be eliminated
SELECT DISTINCT EMP.city AS employee_city
, ORD.ship_city AS order_city
, CUS.city AS customer_city
FROM employees EMP
JOIN orders ORD ON ORD.employee_id = EMP.employee_id
JOIN customers CUS ON CUS.customer_id = ORD.customer_id

-- Task 4.5 Show the lists the product names that order quantity equals to 100.
SELECT PRO.product_name
FROM products PRO
JOIN order_details OD ON OD.product_id = PRO.product_id
WHERE OD.quantity = 100
--
SELECT PRO.product_name
FROM products PRO
WHERE PRO.product_id = any (
	SELECT OD.product_id
	FROM order_details OD
	WHERE OD.quantity = 100
)

-- Task 5.1 Show the lists the suppliers with a product price equal to 10$.
SELECT SUP.contact_name
FROM suppliers SUP
WHERE SUP.supplier_id IN (
	SELECT PRO.supplier_id
	FROM products PRO
	WHERE PRO.unit_price = 10
)

-- Task 5.2 Show the list of employee that perform orders with freight 1000.
SELECT EMP.first_name || ' ' || EMP.last_name AS employee_name
FROM employees EMP
WHERE EMP.employee_id IN (
	SELECT ORD.employee_id
	FROM orders ORD
	WHERE ORD.freight >= 1000
)

-- Task 5.3 Find the Companies that placed orders in 1997
SELECT CUS.company_name
FROM customers CUS
WHERE CUS.customer_id IN (
	SELECT ORD.customer_id
	FROM orders ORD
	WHERE DATE_PART('year', ORD.order_date) = 1997
)

-- Task 5.4 Create a report that shows all products by name that are in the Seafood category.
SELECT PRO.product_name
FROM products PRO
WHERE PRO.category_id IN (
	SELECT CAT.category_id
	FROM categories CAT
	WHERE CAT.category_name LIKE 'Seafood'
)

-- Task 5.5 Create a report that shows all companies by name that sell products in the Dairy Products category.
SELECT SUP.company_name
FROM suppliers SUP
WHERE SUP.supplier_id = any (
	SELECT PRO1.supplier_id
	FROM products PRO1
	WHERE PRO1.category_id = any (
		SELECT CAT2.category_id
		FROM categories CAT2
		WHERE CAT2.category_name = 'Dairy Products'
	)
)

-- Homework 1. Create a report that shows the product name and supplier id for all products supplied by Exotic Liquids, Grandma Kelly's Homestead, and Tokyo Traders.
SELECT PRO.product_name
, PRO.supplier_id
FROM products PRO
WHERE PRO.supplier_id = any (
	SELECT SUP1.supplier_id
	FROM suppliers SUP1
	WHERE SUP1.company_name IN ('Exotic Liquids', 'Tokyo Traders', 'Grandma Kelly_s Homestead')
)

-- Homework 2. Create a report that shows all the products which are supplied by a company called ‘Pavlova, Ltd.’.
SELECT PRO.product_name
FROM products PRO
WHERE PRO.supplier_id = any (
	SELECT SUP1.supplier_id
	FROM suppliers SUP1
	WHERE SUP1.company_name LIKE 'Pavlova, Ltd.'
)

-- Homework 3. Create a report that shows the orders placed by all the customers excluding the customers who belongs to London city.
SELECT *
FROM orders ORD
WHERE ORD.customer_id = any (
	SELECT customer_id
	FROM customers CUS1
	WHERE CUS1.city NOT LIKE 'London'
)

-- Homework 4. Create a report that shows all the customers if there are more than 30 orders shipped in London city.
SELECT CUS.company_name
FROM customers CUS
JOIN orders ORD ON ORD.customer_id = CUS.customer_id
WHERE ORD.ship_city LIKE 'London' 
GROUP BY CUS.company_name
HAVING COUNT(order_id) > 3

-- Homework 5. Create a report that shows all the orders where the employee’s city and order’s ship city are same.
SELECT *
FROM orders ORD
WHERE ORD.order_id IN (
	SELECT order_id
	FROM order_details OD1
	WHERE OD1.product_id IN (
		SELECT PRO2.product_id
		FROM products PRO2
		WHERE PRO2.supplier_id IN (
			SELECT SUP3.supplier_id
			FROM suppliers SUP3
			WHERE SUP3.city LIKE ORD.ship_city
		)
	)
)
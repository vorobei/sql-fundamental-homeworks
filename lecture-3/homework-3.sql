-- 1. Create a report showing the first and last name of all sales representatives who are from  Seattle or Redmond.
SELECT contact_name
FROM orders
JOIN customers
ON orders.customer_id = customers.customer_id
WHERE city in ('Seattle', 'Redmond')
GROUP BY contact_name

-- 2. Create a report that shows the company name, contact title, city and country of all  customers in Mexico or in any city in Spain except Madrid.
SELECT company_name
, contact_title
, city
, country
FROM customers
WHERE country IN ('Mexico', 'Spain') AND city != ('Madrid')

-- 3. Show first and last names of the employees as well as the count of orders each of them have received during the year 1997.
SELECT first_name
, last_name
, count(last_name)
FROM employees
NATURAL JOIN orders
WHERE date_part('year', order_date) = 1997
GROUP BY first_name, last_name

-- Show first and last names of the employees as well as the count of their orders shipped after required date during the year 1997.
SELECT concat(E.first_name, ' ', E.last_name) AS full_name
, count(O.order_id) AS amount_of_orders
FROM employees E
INNER JOIN orders O
ON E.employee_id = O.employee_id
WHERE date_part('year', O.order_date) = 1997 AND O.shipped_date > O.required_date
GROUP BY full_name

-- Create a report showing the information about employees and orders, whenever they had orders or not.
SELECT concat(E.first_name, ' ', E.last_name) AS full_name
, count(O.order_id) AS amount_of_orders
FROM employees E
LEFT JOIN orders O
ON E.employee_id = O.employee_id
GROUP BY full_name
ORDER BY 2

-- Show the list of French customers’ names who used to order non-French products.
SELECT C.company_name
, S.Country as origin_of_products
FROM suppliers S
INNER JOIN products P
ON S.supplier_id = P.supplier_id
INNER JOIN order_details OD
ON P.product_id = OD.product_id
INNER JOIN orders O
ON OD.order_id = O.order_id
INNER JOIN customers C
ON O.customer_id = C.customer_id
WHERE C.country LIKE 'France' AND S.country NOT LIKE 'France'

-- Show the list of suppliers, products and its category.
SELECT contact_name
, product_name
, category_name
FROM suppliers
JOIN products
ON products.supplier_id = suppliers.supplier_id
JOIN categories
ON products.category_id = categories.category_id
-- NATURAL JOIN products
-- NATURAL JOIN categories

-- Create a report that shows all  information about suppliers and products.
SELECT * 
FROM suppliers
NATURAL JOIN products
-- JOIN products ON products.supplier_id = suppliers.supplier_id

-- Show the list of French customers’ names who are working in the same cities.
SELECT C1.company_name
, C1.city
FROM customers C1
JOIN customers C2
ON C1.city = C2.city
WHERE C1.country LIKE 'France' AND C1.company_name != C2.company_name

-- Show the list of German suppliers’ names who are not working in the same cities.
SELECT DISTINCT C1.company_name
FROM suppliers C1
JOIN suppliers C2
ON C1.city != C2.city
WHERE C1.country IN ('Germany') AND C2.country IN ('Germany')

-- Show the count of orders made by each customer from France.
SELECT COUNT(customers.customer_id) AS count_of_orders
FROM customers
JOIN orders
ON customers.customer_id = orders.customer_id
WHERE customers.country IN ('France')


-- Show the list of French customers’ names who have made more than one order.
SELECT COUNT(customers.customer_id)
FROM customers
JOIN orders
ON customers.customer_id = orders.customer_id
WHERE customers.country IN ('France')
GROUP BY customers.customer_id
HAVING COUNT(customers.customer_id) > 5

-- Show the list of customers’ names who used to order the ‘Tofu’ product.
SELECT DISTINCT C.contact_name
FROM customers C
JOIN orders O ON C.customer_id = O.customer_id
JOIN order_details OD ON O.order_id = OD.order_id
JOIN products P ON OD.product_id = P.product_id
WHERE P.product_name LIKE 'Tofu'

-- Show the list of French customers’ names who used to order non-French products.
SELECT DISTINCT C.company_name
FROM customers C
JOIN orders O ON C.customer_id = O.customer_id
JOIN order_details OD ON O.order_id = OD.order_id
JOIN products P ON OD.product_id = P.product_id
JOIN suppliers S ON P.supplier_id = S.supplier_id
WHERE C.country LIKE 'France' AND S.country NOT LIKE 'France'


-- Show the list of French customers’ names who used to order French products.
SELECT DISTINCT C.company_name
FROM customers C
JOIN orders O ON C.customer_id = O.customer_id
JOIN order_details OD ON O.order_id = OD.order_id
JOIN products P ON OD.product_id = P.product_id
JOIN suppliers S ON P.supplier_id = S.supplier_id
WHERE C.country IN ('France') AND S.country IN ('France')

-- Show the total ordering sum calculated for each country of customer.
SELECT country
, ROUND(SUM(unit_price * quantity)::numeric, 2) AS total_price
FROM customers C
JOIN orders O ON C.customer_id = O.customer_id
JOIN order_details OD ON O.order_id = OD.order_id
GROUP BY C.country

-- Show the total ordering sums calculated for each customer’s country for domestic and non-domestic products separately (e.g.: “France – French products ordered – Non-french products ordered” and so on for each country).
SELECT C.country
, ROUND(SUM(OD.unit_price * quantity)::numeric, 2) AS total_price
, 'From this country' AS location
FROM customers C
JOIN orders O ON C.customer_id = O.customer_id
JOIN order_details OD ON O.order_id = OD.order_id
JOIN products P ON OD.product_id = P.product_id
JOIN suppliers S ON P.supplier_id = S.supplier_id
WHERE C.country = S.country
GROUP BY C.country
UNION ALL
SELECT C.country
, ROUND(SUM(OD.unit_price * quantity)::numeric, 2) AS total_price
, 'From another country' AS location
FROM customers C
JOIN orders O ON C.customer_id = O.customer_id
JOIN order_details OD ON O.order_id = OD.order_id
JOIN products P ON OD.product_id = P.product_id
JOIN suppliers S ON P.supplier_id = S.supplier_id
WHERE C.country != S.country
GROUP BY C.country

-- Show the list of product categories along with total ordering sums calculated for the orders made for the products of each category, during the year 1997.
SELECT category_name
, ROUND(SUM(OD.unit_price * quantity)::numeric, 2) AS total_price
FROM orders O
JOIN order_details OD ON O.order_id = OD.order_id
JOIN products P ON OD.product_id = P.product_id
JOIN categories C ON P.category_id = C.category_id
WHERE date_part('year', order_date) = 1997
GROUP BY category_name
ORDER BY total_price DESC

-- Show the list of product names along with unit prices and the history of unit prices taken from the orders (show ‘Product name – Unit price – Historical price’). The duplicate records should be eliminated. If no orders were made for a certain product, then the result for this product should look like ‘Product name – Unit price – NULL’. Sort the list by the product name.
SELECT DISTINCT product_name
, P.unit_price 
, OD.unit_price AS historical_price
FROM orders O
JOIN order_details OD ON O.order_id = OD.order_id
JOIN products P ON OD.product_id = P.product_id
ORDER BY product_name
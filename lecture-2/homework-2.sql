-- Write a query to get Product name and quantity/unit.
SELECT product_name
, units_in_stock
FROM products

-- Write a query to get current Product list (Product ID and name).
SELECT product_id
, product_name
FROM products

-- Write a query to get discontinued Product list (Product ID and name).
SELECT product_id
, product_name
FROM products
WHERE discontinued >= 1

-- Write a query to get most expense and least expensive Product list (name and unit price).
SELECT product_name
, unit_price AS price
FROM products
WHERE unit_price = (SELECT MIN(unit_price) FROM products) 
   OR unit_price = (SELECT MAX(unit_price) FROM products)

-- Write a query to get Product list (id, name, unit price) where current products cost less than $20.
SELECT product_id
, product_name
, unit_price AS price
FROM products
WHERE unit_price <= 20

-- Write a query to get Product list (id, name, unit price) where products cost between $15 and $25.
SELECT product_id
, product_name
, unit_price AS price
FROM products
WHERE unit_price BETWEEN 15 AND 25

-- Write a query to get Product list (name, unit price) of above average price.
SELECT product_name
, unit_price AS price
FROM products
WHERE unit_price > (SELECT AVG(unit_price) FROM products);

-- Write a query to get Product list (name, unit price) of ten most expensive products.
SELECT product_name
, unit_price AS price
FROM products
ORDER BY unit_price DESC
LIMIT 10

-- Write a query to count current and discontinued products.
SELECT SUM(discontinued) AS sum_discontinued
, SUM(units_in_stock)
FROM products

-- Write a query to get Product list (name, units on order , units in stock) of stock is less than the quantity on order.
SELECT product_name
, units_in_stock
, units_on_order
FROM products
WHERE units_on_order > units_in_stock

-- Extra 1. Write a query to get product price in UAH, USD, EUR
SELECT product_name
, unit_price || ' UAH' AS uah_price
, ROUND((unit_price / 40)::numeric, 2) || ' USD' AS usd_price
, ROUND((unit_price / 41)::numeric, 2) || ' EUR' AS eur_price
FROM products

-- Extra 2. Write a query to get all employees who have birthday in the current month
SELECT *
FROM employees
WHERE date_part('month', birth_date) = date_part('month', now())

-- Extra 3. Write a query to get all employees who have birthday in the next week
SELECT birth_date
, date_part('doy', birth_date) as doy_birth_day
, date_part('doy', now()) as doy_now
FROM employees
WHERE date_part('doy', birth_date) >= date_part('doy', now())
  AND date_part('doy', birth_date) <= date_part('doy', now()) + 6